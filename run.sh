#!/bin/bash

echo Uploading Application container 
docker-compose up -d

echo Copying the configuration example file
docker exec -it mobly-app cp .env.example .env

echo Install dependencies
docker exec -it mobly-app composer install

echo Generate key
docker exec -it mobly-app php artisan key:generate

docker exec -it mobly-app mkdir /var/www/app/storage/products
docker exec -it mobly-app chmod -R 755 /var/www/app/storage/products
docker exec -it mobly-app chmod -R 755 /var/www/app/storage/framework

echo Make migrations
docker exec -it mobly-app php artisan migrate

echo Make seeds
docker exec -it mobly-app php artisan db:seed --class=CategoriesTableSeeder
docker exec -it mobly-app php artisan db:seed --class=ProductsTableSeeder

echo Information of new containers
docker ps -a 