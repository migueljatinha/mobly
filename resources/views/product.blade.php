@extends('layouts.app')

@section('content')
<div class="container">

    @include('partials.search')

    <div class="row">
    @if (isset($product))

        @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

        <div class="col-12">
            <h1>{{$product->name}}</h1>
        </div>

        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="card align-self-stretch">
                <img class="card-img-top" src="{!! route('product.image', [$product->image]) !!}" alt="Card image cap">
            </div>
        </div>

        <div class="col-12 col-md-6 col-lg-8 mb-4">
                
                    <div>
                    <h5>
                        {{$product->price_formated}}
                    </h5>
                    @foreach ($product->categories as $category)
                        <span class="badge badge-info">{{$category->name}}</span>
                    @endforeach
                    </div>
                    <p class="card-text">{{$product->description}}</p>
                    <form action="{!! route('product.buy', [str_slug($product->name)]) !!}" method="POST">
                        @csrf
                        <input type="hidden" name="product_id" value="{{$product->id}}">
                        <button type="submit" onclick="submit();" class="btn btn-success mr-2 mb-2" title="Comprar">Comprar</button>
                    </form>
                    <form action="{!! route('cart.add', [str_slug($product->name)]) !!}" method="POST">
                        @csrf
                        <input type="hidden" name="product_id" value="{{$product->id}}">
                        <button type="submit" onclick="submit();" class="btn btn-primary mr-2 mb-2" title="Adicionar ao carrinho de compras"><i class="fas fa-cart-arrow-down"></i></button>
                    </form>

        </div>
    @endif
    </div>

</div>
@endsection
