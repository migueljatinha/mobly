@extends('layouts.app')

@section('content')
<div class="container">

    @include('partials.search') 

    <div class="row">

        <div class="col-12">

            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif

            <div class="row">

            @if( Session::has("searchTerm") )
                <div class="col-12">
                    <div class="alert alert-success">
                        <p class="m-0 p-0">Resultados encontrados para: {{ Session::get("searchTerm") }}</p>
                    </div>
                </div>
            @endif
            
           
                <div class="col-12">
                    
                    <table class="table table-hover">
                        <thead class="thead-dark">
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col"></th>
                            <th scope="col">Produto</th>
                            <th scope="col"></th>
                            <th scope="col">Valor</th>
                            <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($carts as $cart)
                                @foreach ($cart->items as $item)
                                <tr>
                                    <th scope="row">{{$item->id}}</th>
                                    <td>
                                        <a href="{!! route('product', [$item->product->id, str_slug($item->product->name)]) !!}">
                                            <img class="card-img-top" style="width: 30px;" src="{!! route('product.image', [$item->product->image]) !!}" alt="{{$item->product->name}}">
                                        </a>
                                    </td>
                                    <td>{{$item->product->name}}</td>
                                    <td class="text-center"></td>
                                    <td>{{$item->price_formated}}</td>
                                    <td class="text-right">
                                        <form action="{!! route('cart.remove', [str_slug($item->product->name)]) !!}" method="POST">
                                            @csrf
                                            <input type="hidden" name="item_id" value="{{$item->id}}">
                                            <input type="hidden" name="cart_id" value="{{$cart->id}}">
                                            <button type="submit" onclick="submit();" class="btn btn-danger mr-0 mb-0" title="Remover do carrinho de compras"><i class="far fa-trash-alt"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col">Total:</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </tfoot>
                    </table>

                </div>

                <div class="col-12 my-3">
                    <form action="{!! route('cart.order') !!}" method="POST">
                        @csrf
                        <input type="hidden" name="cart_id" value="{{$cart->id}}">
                        <button type="submit" onclick="submit();" class="btn btn-success btn-lg btn-block" title="Finalizar Compra"><i class="fas fa-credit-card"></i> Finalizar Compra</button>
                    </form>
                </div>
            
            </div>
        </div>

    
    </div>

</div>
@endsection
