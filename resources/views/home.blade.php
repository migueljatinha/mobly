@extends('layouts.app')

@section('content')
<div class="container">

    @include('partials.search') 

    <div class="row">

        <div class="col-3 d-none d-md-block">
            <h3>Categorias</h3>
            <ul class="nav nav-pills nav-fill flex-column">
                @foreach ($categories as $category)
                <li class="nav-item text-left">
                    <a class="nav-link {!! isset($current_category) && $current_category == $category->id ? 'active' : '' !!}" href="{!! route('product.category', [$category->id, str_slug($category->name)]) !!}">
                        {{$category->name}}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>

        <div class="col-12 col-md-9">

            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            <div class="row">

            @if( Session::has("searchTerm") )
                <div class="col-12">
                    <div class="alert alert-success">
                        <p class="m-0 p-0">Resultados encontrados para: {{ Session::get("searchTerm") }}</p>
                    </div>
                </div>
            @endif
            
            @foreach ($products as $product)
                <div class="col-12 col-md-6 col-lg-4 mb-4">
                    @include('partials.product-card') 
                </div>
            @endforeach
            
            </div>
        </div>

    
    </div>

    <!-- <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div> -->

</div>
@endsection
