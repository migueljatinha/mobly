                    <div class="card align-self-stretch">
                        <a href="{!! route('product', [$product->id, str_slug($product->name)]) !!}">
                            <img class="card-img-top" src="{!! route('product.image', [$product->image]) !!}" alt="{{$product->name}}">
                        </a>
                        
                        <div class="card-body">
                            <h5 class="card-title">{{$product->name}}</h5>
                            <h3>
                                {{$product->price_formated}}
                            </h3>
                            <div>
                            @foreach ($product->categories as $category)
                                <span class="badge badge-info">{{$category->name}}</span>
                            @endforeach
                            </div>
                            <p class="card-text">{{$product->description}}</p>
                            <form action="{!! route('product.buy', [str_slug($product->name)]) !!}" method="POST">
                                @csrf
                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                <button type="submit" onclick="submit();" class="btn btn-success mr-2 mb-2" title="Comprar">Comprar</button>
                            </form>
                            <a href="{!! route('product', [$product->id, str_slug($product->name)]) !!}" title="Detalhes do produto" class="btn btn-success mr-2 mb-2">
                                <i class="fas fa-search-plus"></i>
                            </a>
                            <form action="{!! route('cart.add', [str_slug($product->name)]) !!}" method="POST">
                                @csrf
                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                <button type="submit" onclick="submit();" class="btn btn-primary mr-2 mb-2" title="Adicionar ao carrinho de compras"><i class="fas fa-cart-arrow-down"></i></button>
                            </form>
                        </div>
                    </div>