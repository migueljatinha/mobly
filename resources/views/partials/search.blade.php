<div class="jumbotron p-3">
    <div class="container">
        <form action="{!! route('product.search') !!}" method="POST">
            @csrf
            <h3 >Encontre o que procura</h3>
            <div class="input-group mb-3">
            
                <input type="text" name="term" class="form-control" placeholder="Buscar produto" aria-label="Buscar produto" aria-describedby="button-addon2">
                <div class="input-group-append">
                    <button class="btn btn-success" type="submit" id="button-addon2"><i class="fas fa-search"></i></button>
                </div>
                
            </div>
        </form>
    </div>
</div>