<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            // $table->engine = "InnoDB";
            $table->increments('id')->unsigned();
            
            $table->integer('price')->unsigned();
            
            $table->integer('product_id')->unsigned();
            // $table->integer('cart_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products');
            // $table->foreign('cart_id')->references('id')->on('carts');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
