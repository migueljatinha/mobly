<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_item', function (Blueprint $table) {
            
            // $table->engine = "InnoDB";

            $table->integer('item_id')->unsigned();
            $table->integer('cart_id')->unsigned();

            $table->foreign('item_id')
                ->references('id')->on('items')
                ->onDelete('cascade');
                
            $table->foreign('cart_id')
                ->references('id')->on('categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories_item');
    }
}
