<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\Category;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('pt_BR');

        $categories = Category::pluck('id')->all();

        for ($i = 0; $i < 100; $i++) 
        {
            $product = Product::create(
                [
                    'name' => 'Produto '.$i, //$faker->company,
                    'description' => $faker->text,
                    'image' => $faker->image( storage_path() . '/products',400,400, null, false),
                    'price' => $faker->randomNumber(2),
                ]
            );

            if (count($categories))
            {
                for ($j = 0; $j < rand(0, 4); $j++) 
                {
                    $product->categories()->attach( $categories[ rand(0, count($categories) - 1 ) ] );
                }
            }
        }
    }
}
