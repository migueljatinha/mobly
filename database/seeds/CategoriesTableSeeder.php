<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = [
            'Quarto',
            'Cozinha',
            'Banheiro',
            'Varanda',
            'Sala de Estar',
            'Sala de Jantar',
            'Lavanderia',
            'Escritório'
        ];

        foreach ($categories as $category)
        {
            Category::create(['name' => $category]);
        }
    }

}
