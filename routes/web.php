<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/',                                 'HomeController@index')         ->name('home');

Route::get('/carrinho-de-compras',              'CartController@index')         ->middleware('auth')->name('cart');
Route::post('/adicionar-ao-carrinho/{name?}',   'CartController@add')           ->name('cart.add');
Route::post('/remover-do-carrinho/{id?}',       'CartController@remove')        ->name('cart.remove');
Route::post('/comprar/{name?}',                 'ProductController@buy')        ->name('product.buy');

Route::post('/finalizar-compra',                'CartController@order')         ->name('cart.order');

Route::get('/categoria/{id?}/{name?}',          'CategoryController@index')     ->name('product.category');
Route::get('/produto/image/{filename?}',        'ProductController@image')      ->name('product.image');
Route::post('/busca',                           'ProductController@search')     ->name('product.search');
Route::get('/{id}/{name}',                      'ProductController@index')      ->name('product');