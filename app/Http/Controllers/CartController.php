<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Item;
use App\Cart;
use Session;
use Auth;

use App\Providers\HelperServiceProvider as Helper;

class CartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    
    public function index()
    {
        $session_id = Session::getId();
        $user = Auth::user();

        $carts = Cart::
                      where('confirmed', 0)
                    ->where('user_id', $user->id)
                    ->where('session_id', $session_id)
                    ->orderBy('created_at', 'DESC')
                    ->with('items')
                    ->with('items.product')
                    ->get()
                    ;

        $categories = Category::all();
        return view('cart', ['carts' => $carts, 'categories' => $categories]);
    }
    
    public function add($name, Request $request)
    {
        $product = Product::findOrFail($request->get('product_id'));
        Helper::insertInCart($product);

        return redirect()->route('home')->with('message', $product->name . ' adicionado ao carrinho com sucesso!');
    }

    public function remove($name, Request $request)
    {

        $item = Item::findOrFail($request->get('item_id'));
        $cart = Cart::findOrFail($request->get('cart_id'));

        $cart->items()->detach( $item->id );
        
        $cart_items =  $cart->items()->count();
        Session::put(["cart_items" => $cart_items]);

        return redirect()->route('cart')->with('message', $item->product->name . ' removido do carrinho com sucesso!');
    }

    public function order(Request $request)
    {

        $cart = Cart::findOrFail($request->get('cart_id'));

        $cart->confirmed = 1;
        $cart->save();
        
        Session::put(["cart_items" => 0]);

        return redirect()->route('home')->with('message', 'Compra finalizada com sucesso!');
    }
}
