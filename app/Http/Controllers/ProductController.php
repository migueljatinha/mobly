<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use File;
use Response;
use Session;

use App\Providers\HelperServiceProvider as Helper;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function index($id, $name)
    {
        $product = Product::findOrFail($id);
        return view('product', ['product' => $product]);
    }

    // This method is usefull in case of implements something like a CDN or in case of needs URLs like /product/image.jpg?width=100
    public function image($filename) 
    {
        $path = storage_path('products/' . $filename);

        if (!File::exists($path)) {
            abort(404);
        }
    
        $file = File::get($path);
        $type = File::mimeType($path);
    
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
    
        return $response;
    }

    public function search(Request $request)
    {
        $term = $request->input('term');

        Session::flash('searchTerm', $term);
        
        $products = Product::where('name', 'like', '%'.$term.'%')->get();
        $categories = Category::all();
        return view('home', ['products' => $products, 'categories' => $categories, '']);
    }

    public function buy($name, Request $request)
    {
        $product = Product::findOrFail($request->get('product_id'));
        Helper::insertInCart($product);

        return redirect()->route('cart')->with('message', $product->name . ' adicionado ao carrinho com sucesso!');
    }

}
