<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use App\Cart;
use Auth;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $old_session_id = Session::getId();

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            // Authentication passed...
            $session_id = Session::getId();
            $user = Auth::user();

            Cart::
                where('confirmed', 0)
                ->where('session_id', $old_session_id)
                ->update(['user_id' => $user->id, 'session_id' => $session_id]);

            return redirect()->route('cart');
        }

        return redirect()->route('login')->with('error', 'Usuário ou senha inválidos!');
    }
}
