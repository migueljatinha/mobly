<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        $products = Product::limit(9)->inRandomOrder()->orderBy('name')->get();
        $categories = Category::all();
        return view('home', ['products' => $products, 'categories' => $categories]);
    }
}
