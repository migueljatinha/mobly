<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use File;
use Response;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function index($id, $name)
    {
        $categories = Category::all();
        $products = Product::with('categories')
            ->whereHas('categories', function($query) use ($id) {
                $query->where('id', $id);
            })->get();
        return view('home', ['products' => $products, 'categories' => $categories, 'current_category' => $id]);
    }

}
