<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Product;
use App\Category;
use App\Item;
use App\Cart;
use Session;
use Auth;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function getFormatMoney($number) 
    {
        // Needs to enable the extension in php.ini by uncommenting the line ;extension=php_intl.dll
        // $formatter = new NumberFormatter('pt_BR',  NumberFormatter::CURRENCY);
        // return $formatter->formatCurrency($this->price, 'BRL');

        // return money_format ('%i',$number);

        //Working around
        return 'R$ '.number_format ($number, 2, ',', '.');
    }

    public static function insertInCart(Product $product)
    {
        $session_id = Session::getId();
        $user = Auth::user();

        $cart = Cart::orderBy('updated_at')
                    ->where('confirmed', 0)
                    ->where('session_id', $session_id)
                    ->where('user_id', object_get($user, 'id', null) )
                    ->orderBy('created_at', 'DESC')
                    ->firstOrCreate([
                        'user_id' => object_get($user, 'id'),
                        'session_id' => $session_id
                    ])
                    ;
        
        $item = Item::create([
            'price'         => $product->price, //Prevent price changes after put in a Cart
            'product_id'    => $product->id,
        ]);
        
        $cart->items()->attach( $item );
        
        $cart_items =  $cart->items()->count();
        Session::put(["cart_items" => $cart_items]);

        return true;
    }
}
