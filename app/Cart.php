<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    
    protected $hidden = 
    [
        'created_at', 'updated_at',
    ];

    protected $fillable =
    [
        'user_id',
        'session_id'
    ];

    /**
     * Get the products for the category.
     */
    public function items()
    {
        return $this->belongsToMany('App\Item');
    }
}
