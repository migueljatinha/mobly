<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'categories';
    
    protected $fillable =
    [
        'name'
    ];

    protected $hidden = 
    [
        'created_at', 'updated_at',
    ];

    /**
     * Get the products for the category.
     */
    public function products()
    {
        return $this->hasMany('App\Product');
    }

}
