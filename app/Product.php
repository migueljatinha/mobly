<?php

namespace App;
use App;

use Illuminate\Database\Eloquent\Model;
use App\Providers\HelperServiceProvider as Helper;

class Product extends Model
{
    protected $fillable =
    [
        'name',
        'description',
        'image',
        'price'
    ];

    protected $hidden = 
    [
        'created_at', 'updated_at',
    ];

    /**
     * Get the categories for the product.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function getImagePathAttribute()
    {
        return storage_path('products/'.$this->image);
    }

    public function getPriceFormatedAttribute()
    {
        return Helper::getFormatMoney($this->price);
    }

}
