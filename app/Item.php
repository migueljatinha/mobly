<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\Providers\HelperServiceProvider as Helper;

class Item extends Model
{
    protected $fillable =
    [
        'price',
        'product_id'
    ];

    protected $hidden = 
    [
        'created_at', 'updated_at',
    ];

    public function cart()
    {
        return $this->belongsTo('App\Cart');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function getPriceFormatedAttribute()
    {
        return Helper::getFormatMoney($this->price);
    }
}
