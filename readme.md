# How to test the project

There are at least two ways to run this project:

# 1. Via Docker

Open a bash prompt window and type the following commands

Uploading Application container:
```bash
$ docker-compose up -d
```

Copying the configuration example file
```bash
$ docker exec -it mobly-app cp .env.example .env
```

Install dependencies
```bash
$ docker exec -it mobly-app composer install
```

Generate key
```bash
$ docker exec -it mobly-app php artisan key:generate
```

Make storage writable to save products images using seeder and sessions data
```bash
$ docker exec -it mobly-app mkdir /var/www/app/storage/products
$ docker exec -it mobly-app chmod -R 755 /var/www/app/storage/products
$ docker exec -it mobly-app chmod -R 755 /var/www/app/storage/framework
```

Make migrations
```bash
$ docker exec -it mobly-app php artisan migrate
```

Make seeds
```bash
$ docker exec -it mobly-app php artisan db:seed --class=CategoriesTableSeeder
$ docker exec -it mobly-app php artisan db:seed --class=ProductsTableSeeder
```

Information of new containers
```bash
$ docker ps -a 
```

Access
[Docker Localhost](http://127.0.0.1:81/)

# 2. Via php artisan

First at all, you must have a MySQL server running on computer and created a database for the application.

Open .env.example and change the access for your database as follow:
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=mobly
DB_USERNAME=mobly
DB_PASSWORD=mobly

Then open a bash prompt window, access the folder of the application and type the following commands:

Copying the configuration example file
```bash
$ cp .env.example .env
```

Install composer dependencies
```bash
$ composer install
```

Install npm dependencies
```bash
$ npm install
```

Generate key
```bash
$ php artisan key:generate
```

Make storage writable to save products images using seeder and sessions data
```bash
$ mkdir /var/www/app/storage/products
$ chmod -R 755 /var/www/app/storage/products
$ chmod -R 755 /var/www/app/storage/framework
```

Make migrations
```bash
$ php artisan migrate
```

Make seeds
```bash
$ php artisan db:seed --class=CategoriesTableSeeder
$ php artisan db:seed --class=ProductsTableSeeder
```

Access
[Localhost](http://localhost/)

## Atention

This two commands bellow are only used by me on my home environment

To stop all Docker containers
```bash
docker stop $(docker ps -a -q)
```

To remove all Docker containers
```bash
docker rm $(docker ps -a -q)
```

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
